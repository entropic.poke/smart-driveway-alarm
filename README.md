# Smart Driveway (Perimeter) Alarm

Can't find a driveway or perimeter alarm that integrates with your Smart Home?  Then build it yourself.  Here's how...

I have built 2 of the 4 sensor units for a total of 8 sensors surrounding the outside of my home.  The sensor/transmitters can be up to about 1/2 mile away from the receiver and still work.  When something/someone triggers a motion sensor it will do the following:
- send a radio signal to the receiver.
- the receiver will play an alert tone.
- the receiver will also turn on the corresponding built in relay for that zone.
- when this relay is turned on it will connect power to the electromagnet
- when magnetized the electromagnet will trigger the door/window sensor to change from open to closed status for about 10 seconds.
- when the sensor status changes you can set automation in your smart home system to perform what ever action is desired, turn on lights, text or message you, etc.

This solution requires a Smart Home system like Smartthings or Hubitat...

For the diagrams and instructions see the included [PowerPoint](https://gitlab.com/entropic.poke/smart-driveway-alarm/-/blob/master/Smart_Driveway_Alarm.pptx) slides. 

Also include are the STL files for either a single sensor or 4 sensors.  These STL files are fitted to the Uxcell 12v 25N electromagnets and the Smartthings Door/Window sensors.

links to Amazon products
[HTZ Receiver](https://www.amazon.com/HTZSAFE-Wireless-Receiver-Multifunctional-Driveway/dp/B06XKC3B56?ref_=ast_sto_dp)
    - it doesnt show it in the picture, but in the description this item actually has NO/NC relays for each of its 4 zones.
    - see the [user manual](https://images-na.ssl-images-amazon.com/images/I/B1RaOUDa-OS.pdf) for more details.



[HTZ Solar Detector Transmitter](urlhttps://www.amazon.com/HTZSAFE-Wireless-Receiver-Multifunctional-Driveway/dp/B06XKC3B56?ref_=ast_sto_dp)
    - There are several to choose from... it doesnt matter, it will work as long as it is from HTZ Safe.
    - I use the 50ft motion sensor with solar panel charger, but I have read good things about the 300ft infrared detectors too.
     

[12v 25N electromagnet (1 or 4)](https://www.amazon.com/s?k=electromagnet+12v+25n&ref=nb_sb_noss)
    - It doesnt matter what brand, I have used Uxcel and other brands.  They all appear to be the same size.  You do not want one on a PCB, just the cylinder with the wires coming out of it to fit into the 3d printed module.
     

[Smartthings Hub v.3](https://www.amazon.com/Samsung-SmartThings-Generation-GP-U999SJVLGDA-Automation/dp/B07FJGGWJL/ref=sr_1_3?dchild=1&keywords=smartthings+hub&qid=1600736593&sr=8-3)
    - If you dont already have one...
     
[Smartthings Door and Window Multisensor (1 or 4)](https://www.amazon.com/Samsung-SmartThings-Multipurpose-Sensor-GP-U999SJVLAAA/dp/B07F956F3B/ref=sr_1_3?dchild=1&keywords=smartthings+multisensor&qid=1600735671&sr=8-3)
    - I recommend the Smartthings Door and Windows Multisensor but just about any door or windows sensor will do.  The 3D print job is designed for the ST Multisensor.
     
[12volt 500ma AC to DC power supply](https://www.amazon.com/BENSN-Universal-Adapter-Security-Portable/dp/B075VQ7NQH/ref=sr_1_1_sspa?dchild=1&keywords=12v+500ma+power+supply&qid=1600739007)
    - each electromagnet only consumes about 100ma during operation.


- a few resistors (220 ohm)